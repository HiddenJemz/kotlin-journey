/*
	Using Kotlin to get user input

*/

fun main(){

	print("Enter name: ")

	val userInput = readLine()!!

	println("Hello $userInput")

	println("What would you like to do today? ")
	var userInput2 = readLine()!!

	val display = when(userInput2.toInt()){

			1 -> println("java -jar when.jar\r")
			else -> println("Exiting... Have a great day")

		}
	println(display)

	//COMPILE: kotlinc userInput.kt -include-runtime -d userIn.jar
	//RUN: java -jar userIn.jar

}
//Successfully completed!
